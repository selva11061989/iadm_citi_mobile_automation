package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class TH_MF_KnE_Objects {
	
	public static final By continue_Android=By.id("com.citi.mobile.th.dit:id/continue_button");
	public static final By allowBtn = By.xpath("//android.widget.Button[@text='ALLOW']");
	public static final By langBtn = By.xpath("//android.widget.TextView[@text='English']");
	public static final By acceptBtn = By.xpath("//android.widget.TextView[@text='Accept']");
	public static final By existingLoginBtn = By.id("com.citi.mobile.th.dit:id/login_button");
	public static final By username_Android=By.xpath("//android.widget.EditText[contains(@text,'User ID')]");
	public static final By password_Android=By.xpath("//android.widget.EditText[@text='Password']");
	public static final By loginButton_Android=By.xpath("//android.widget.Button[@text='Log in']");
	
	public static final By wealth_Android=By.xpath("//android.view.View[@text='Wealth']");
	public static final By bmfChevron=By.xpath("//android.widget.TextView[@text='Buy Mutual Funds']");
	public static final By closeBtn=By.xpath("(//android.view.View[@index='0'])[25]");
	public static final By errorIcon=By.xpath("(//android.view.View[@index='1'])[9]");
	public static final By completeYourInvProf=By.xpath("//android.view.View[@text='Complete your investment profile']");
	public static final By content=By.xpath("//android.view.View[@text='Take a few minutes to update your investement profile before proceeding.']");
	public static final By completeMyProfBtn=By.xpath("//android.widget.Button[@text='Complete my profile']");

}
