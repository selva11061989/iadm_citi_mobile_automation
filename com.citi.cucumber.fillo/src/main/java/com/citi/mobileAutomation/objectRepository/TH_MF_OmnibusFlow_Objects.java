package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class TH_MF_OmnibusFlow_Objects {
	
	public static final By continue_Android=By.id("com.citi.mobile.th.dit:id/continue_button");
	public static final By allowBtn = By.xpath("//android.widget.Button[@text='ALLOW']");
	public static final By langBtn = By.xpath("//android.widget.TextView[@text='English']");
	public static final By acceptBtn = By.xpath("//android.widget.TextView[@text='Accept']");
	public static final By existingLoginBtn = By.id("com.citi.mobile.th.dit:id/login_button");
	public static final By username_Android=By.xpath("//android.widget.EditText[contains(@text,'User ID')]");
	public static final By enter_password_Android=By.xpath("//android.widget.Button[@text='Enter Password']");
	public static final By password_Android=By.xpath("//android.widget.EditText[@text='Password']");
	public static final By loginButton_Android=By.xpath("//android.widget.Button[@text='Log in']");
	public static final By getStarted_Android=By.xpath("//android.widget.TextView[@text='Get started']");
	public static final By enableButton_Android=By.xpath("//android.widget.TextView[@text='Enable']");
	public static final By alert_Ok_Btn_Android=By.xpath("//android.widget.Button[contains(@text,'OK')]");
	public static final By experienceCitiMobile_Android=By.xpath("//android.widget.TextView[contains(@text,'Experience Citi Mobile®')]");
	
	public static final By wealth_Android=By.xpath("//android.view.View[@text='Wealth']");
	public static final By bmfChevron=By.xpath("//android.widget.TextView[@text='Buy Mutual Funds']");
	public static final By omnibusChevron=By.xpath("(//android.view.View[@index='11'])[2]");
	public static final By selectFund=By.xpath("//android.widget.Button[@text='Select fund']");
	public static final By acctSelection=By.xpath("//android.view.View[@text='KXOXG SXI FXN']");
	public static final By forwardBtn=By.xpath("(//android.view.View[@index='3'])[6]");
	public static final By textInput=By.xpath("//android.widget.EditText[@text='0.00']");
	public static final By txtInputForwardBtn=By.xpath("(//android.view.View[@index='15'])[2]");
	public static final By backBtn=By.xpath("(//android.view.View[@index='0'])[19]");
	public static final By header=By.xpath("//android.view.View[@text='Mutual funds account opening']");
	public static final By desc1=By.xpath("//android.view.View[@text='To proceed with your order, we will be opening a Mutual Funds account for you.']");
	public static final By desc2=By.xpath("//android.view.View[@text='By default, all dividends and returns will be credited to the following account(s).']");
	public static final By fundsInXXX=By.xpath("//android.view.View[@text='For funds in THB']");
	public static final By acctNum=By.xpath("//android.view.View[@text='•••• 9876']");
	public static final By acctHeader=By.xpath("//android.view.View[@text='THB account']");
	public static final By closeBtn=By.xpath("(//android.view.View[@index='1'])[8]");
	public static final By accNum=By.xpath("(//android.view.View[@text='•••• 9876'])[2]");				
	public static final By tickSymbol=By.xpath("//android.widget.Image[@text='success']");
	//public static final By disclaimer=By.xpath("//android.view.View[@text='By tapping "Next", I acknowledge that I have read and agree to the Terms & Conditions and Important Document.']");
	public static final By pdfIcon=By.xpath("//android.widget.Image[@text='pdf']");
	public static final By pdfBackBtn=By.xpath("//android.widget.ImageButton[@content-desc='Back button']");
	public static final By termsAndConditions=By.xpath("//android.view.View[@text='Terms and Conditions']");
	public static final By impDoc=By.xpath("//android.view.View[@text='Important Document']");
	public static final By nextButton=By.xpath("//android.widget.Button[@text='Next']");
	
}
