package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class TH_Investments_Recap_Objects {
	
	public static final By continue_Android=By.id("com.citi.mobile.th.dit:id/continue_button");
	public static final By allowBtn = By.xpath("//android.widget.Button[@text='ALLOW']");
	public static final By langBtn = By.xpath("//android.widget.TextView[@text='English']");
	public static final By acceptBtn = By.xpath("//android.widget.TextView[@text='Accept']");
	public static final By existingLoginBtn = By.id("com.citi.mobile.th.dit:id/login_button");
	public static final By username_Android=By.xpath("//android.widget.EditText[contains(@text,'User ID')]");
	public static final By password_Android=By.xpath("//android.widget.EditText[@text='Password']");
	public static final By loginButton_Android=By.xpath("//android.widget.Button[@text='Log in']");
	public static final By getStarted_Android=By.xpath("//android.widget.TextView[@text='Get started']");
	public static final By enableButton_Android=By.xpath("//android.widget.TextView[@text='Enable']");
	public static final By alert_Ok_Btn_Android=By.xpath("//android.widget.Button[@text='OK']");
	public static final By experienceCitiMobile_Android=By.xpath("//android.widget.TextView[contains(@text,'Experience Citi Mobile®')]");
	
	public static final By investmentsdownChevron=By.xpath("(//android.widget.Image[@index='2'])[4]");
	public static final By mutualFundChevron=By.xpath("(//android.widget.Image[@index='0'])[16]");
	public static final By holdingsChevron=By.xpath("(//android.widget.Image[@index='0'])[26]");
	public static final By acctSelection=By.xpath("(//android.widget.RelativeLayout[@index='1'])[2]");
	public static final By ctaSellBtn=By.id("com.citi.mobile.th.dit:id/ctaSellButton");
	public static final By sellBtn=By.xpath("//android.widget.TextView[@text='Sell']");
	public static final By backButton=By.xpath("(//android.view.View[@index='0'])[17]");
	public static final By indicativeNav=By.xpath("//android.view.View[@text='Indicative NAV']");
	public static final By unitsTosell=By.xpath("//android.view.View[@text='How many units do you want to sell?']");
	public static final By indicativeNavamt=By.xpath("//android.view.View[@text='89.53000 as of 14 Nov 2019']");
	public static final By inputAmt=By.xpath("//android.widget.EditText[@text='0.0000']");
	public static final By unitsHeld=By.xpath("//android.view.View[@text='Unit held: 31,334.8657']");
	public static final By estimatedAmt=By.xpath("//android.view.View[@text='Estimated amount from selling']");
	public static final By flagImage=By.xpath("//android.widget.Image[@text='flag_thb']");
	public static final By currencyAndAmt=By.xpath("//android.view.View[@text='THB 0.00']");
	public static final By description=By.xpath("//android.view.View[@text='The amount shown above is the indicative it does not include redemption charge if the fund is applicable.']");
	public static final By forwardBtn=By.xpath("(//android.view.View[@index='0'])[18]");
	//public static final By forwardBtn=By.xpath("//div[@class='mf-sell-round-button-right']");
	
	
	public static final By backBtn=By.xpath("(//android.view.View[@index='0'])[18]");
	public static final By reviewInvestment=By.xpath("//android.view.View[@text='Review and confirm']");
	public static final By closeBtn=By.xpath("(//android.view.View[@index='2'])[5]");
	public static final By sellOrderHeader=By.xpath("//android.view.View[@text='You have a sell order of']");
	public static final By acctName=By.xpath("//android.view.View[@text='(RMF) ABERDEEN STANDARD SMART CAPITAL - RMF (ABSC-RMF)']");
	public static final By redeemTxt=By.xpath("//android.view.View[@text='You have redeemed']");
	public static final By units=By.xpath("//android.view.View[@text='10,000.0000 units']");
	public static final By orderDate=By.xpath("//android.view.View[@text='Order date']");
	public static final By date=By.xpath("//android.view.View[@text='5 Sep 2016']");
	public static final By tradeDisclaimer=By.xpath("//android.view.View[@text='Trade will be completed within 3 working days']");
	public static final By creditToAcctNum=By.xpath("//android.view.View[@text='Credit to account number']");
	public static final By creditNum=By.xpath("//android.view.View[@text='•••• 0045']");
	public static final By acctHeader=By.xpath("//android.view.View[@text='Which account would you like to credit?']");
	public static final By acctNum=By.xpath("//android.view.View[@text='•••• 0063']");
	public static final By closeBtnsecond=By.xpath("(//android.view.View[@index='0'])[21]");
	public static final By investmentMode=By.xpath("//android.view.View[@text='Investment mode']");
	public static final By oneTime=By.xpath("//android.view.View[@text='One time then monthly']");
	public static final By fromTxt=By.xpath("//android.view.View[@text='From']");
	public static final By amountCredited=By.xpath("//android.view.View[@text='Amount credited']");
	public static final By amountVal=By.xpath("//android.view.View[@text='THB 2,000.25']");
	public static final By amtDescription=By.xpath("//android.view.View[@text='The amount shown includes a charge of 1% redemption fee which is applicabe to this mutual fund.']");
	public static final By fundCode=By.xpath("//android.view.View[@text='Fund Code']");
	public static final By acash=By.xpath("//android.view.View[@text='ABSC']");
	public static final By riskRating=By.xpath("//android.view.View[@text='Risk Rating']");
	public static final By riskRatingVal=By.xpath("//android.view.View[@text='4']");
	public static final By availableUnits=By.xpath("//android.view.View[@text='Available units']");
	public static final By availableUnitsVal=By.xpath("//android.view.View[@text='31,334.8657']");
	public static final By indicativeNavRecap=By.xpath("//android.view.View[@text='Indicative NAV']");
	public static final By indicativeNavVal=By.xpath("//android.view.View[@text='89.53000 as of 14 Nov 2019']");
	public static final By fundCurrencyType=By.xpath("//android.view.View[@text='Fund currency type']");
	public static final By fundCurrencyVal=By.xpath("//android.view.View[@text='THB']");
	public static final By transactionType=By.xpath("//android.view.View[@text='Transaction type']");
	public static final By transactionTypeVal=By.xpath("//android.view.View[@text='Non-advised']");
	public static final By disclaimer=By.xpath("//android.view.View[contains(@text,'By tapping')]");
	public static final By closeBtnOfCredit=By.xpath("(//android.view.View[@index='0'])[23]");
	public static final By pdf1=By.xpath("(//android.widget.Image[@text='pdf'])[1]");
	public static final By backBtnLast=By.xpath("//android.widget.ImageButton[@content-desc='Back button']");
	public static final By pdf2=By.xpath("(//android.widget.Image[@text='pdf'])[2]");
	public static final By termsAndConditions=By.xpath("//android.view.View[@text='Terms and Conditions']");
	public static final By disclosureStatement=By.xpath("android.view.View[@text='Disclosure Statement']");
	public static final By confirmBtn=By.xpath("//android.widget.Button[@text='Confirm']");
	public static final By secondForward=By.xpath("(//android.view.View[@index='0'])[16]");
	
	
	
}
