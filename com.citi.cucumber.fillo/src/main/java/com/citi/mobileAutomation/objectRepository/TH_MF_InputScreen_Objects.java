package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class TH_MF_InputScreen_Objects {
	
	public static final By continue_Android=By.id("com.citi.mobile.th.dit:id/continue_button");
	public static final By allowBtn = By.xpath("//android.widget.Button[@text='ALLOW']");
	public static final By langBtn = By.xpath("//android.widget.TextView[@text='English']");
	public static final By acceptBtn = By.xpath("//android.widget.TextView[@text='Accept']");
	public static final By existingLoginBtn = By.id("com.citi.mobile.th.dit:id/login_button");
	public static final By username_Android=By.xpath("//android.widget.EditText[contains(@text,'User ID')]");
	public static final By password_Android=By.xpath("//android.widget.EditText[@text='password']");
	public static final By loginButton_Android=By.xpath("//android.widget.Button[@text='Log in']");
	public static final By getStarted_Android=By.xpath("//android.widget.TextView[@text='Get started']");
	public static final By enableButton_Android=By.xpath("//android.widget.TextView[@text='Enable']");
	public static final By alert_Ok_Btn_Android=By.xpath("//android.widget.Button[@text='OK']");
	public static final By experienceCitiMobile_Android=By.xpath("//android.widget.TextView[contains(@text,'Experience Citi Mobile®')]");
	
	public static final By investmentsdownChevron=By.xpath("(//android.widget.Image[@index='2'])[4]");
	public static final By mutualFundChevron=By.xpath("(//android.widget.Image[@index='0'])[16]");
	public static final By holdingsChevron=By.xpath("(//android.widget.Image[@index='0'])[26]");
	public static final By acctSelection=By.xpath("(//android.widget.RelativeLayout[@index='1'])[2]");
	public static final By sellBtn=By.xpath("//android.widget.TextView[@text='Sell']");
	public static final By backButton=By.xpath("(//android.view.View[@index='0'])[17]");
	public static final By indicativeNav=By.xpath("//android.view.View[@text='Indicative NAV']");
	public static final By unitsTosell=By.xpath("//android.view.View[@text='How many units do you want to sell?']");
	public static final By indicativeNavamt=By.xpath("//android.view.View[@text='89.53000 as of 14 Nov 2019']");
	public static final By inputAmt=By.xpath("//android.widget.EditText[@text='0.0000']");
	public static final By unitsHeld=By.xpath("//android.view.View[@text='Unit held: 31,334.8657']");
	public static final By estimatedAmt=By.xpath("//android.view.View[@text='Estimated amount from selling']");
	public static final By flagImage=By.xpath("//android.widget.Image[@text='flag_thb']");
	public static final By currencyAndAmt=By.xpath("//android.view.View[@text='THB 0.00']");
	public static final By description=By.xpath("//android.view.View[@text='The amount shown above is the indicative it does not include redemption charge if the fund is applicable.']");
	public static final By forwardButton=By.xpath("(//android.view.View[@index='0'])[16]");

}
