package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;



public class A_ReferenceObjects {
	
	/*
	 * @Author:Selva
	 * Product :Prerequiste_Object
	 * Platform:Android
	 * 
	 */
	public static final By continue_And=By.id("com.citi.mobile.sg.dit:id/continue_button");
	public static final By activateCard_And=By.id("com.citi.mobile.sg.dit:id/activatecard_button");
	public static final By loginAccount_And =By.id("com.citi.mobile.sg.dit:id/register_link");
	public static final By loginAccountWrong_And =By.id("/znbcm");
	public static final By existingAcc_And=By.id("com.citi.mobile.sg.dit:id/login_button");
	public static final By userID_And=By.id("com.citi.mobile.sg.dit:id/txtUserID");
	public static final By password_And=By.id("com.citi.mobile.sg.dit:id/txtPassword");
	public static final By login_And=By.id("com.citi.mobile.sg.dit:id/odyssey_login_button");
	/*
	 * @Author:Selva
	 * Product :Prerequiste_Object
	 * Platform:iOS
	 * 
	 */
	
	public static final By activateCard_iOS=By.xpath("//XCUIElementTypeButton[@name=\"Activate card\"]");
	public static final By loginAccount_iOS=By.xpath("//XCUIElementTypeButton[@name=\"New? Create a login account\"]");
	public static final By loginAccountWrong_iOS=By.xpath("//XCUIElementTypeButton[@name=\"\"]");
	public static final By existingAcc_iOS=By.xpath("//XCUIElementTypeButton[@name=\"Log in with existing account\"]");
	public static final By userID_iOS=By.xpath("//XCUIElementTypeStaticText[@name=\"User ID\"]");
	public static final By password_iOS=By.xpath("//XCUIElementTypeStaticText[@name=\"Password\"]");
	public static final By login_iOS=By.xpath("//XCUIElementTypeButton[@name=\"Btn_Signon_230_login\"]");
	
	
//========================================================================================================
	
	//For Your Reference
	
	public static final By userIDTWA =By.xpath("//*[contains(@name,'USER')]");
	public static final By EnterOTP = By.xpath("//*[contains(@id,'authOtpId')]");	
	public static final By SelectCountryTWA =By.xpath("//*[contains(@value,'HK')]");
}	
	
