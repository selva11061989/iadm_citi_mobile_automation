package com.citi.mobileAutomation.autoInvoking;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.citi.mobileAutomation.capabilities.DesiredCap;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class OpeningAppiumServer {

	private static AppiumServiceBuilder builder;

	protected static AppiumDriverLocalService service;
	private static Logger logger = Logger.getLogger(InvokingEmulator.class);

	public static void stopAppiumServer() {
		//service.stop();
		
		Runtime runtime = Runtime.getRuntime();
	    try {
	        runtime.exec("killall node");
	        runtime.exec("killall node");
	        runtime.exec("killall node");
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		System.out.println("server close");
	}

	public static void startAppiumServerSimple() {

		service = AppiumDriverLocalService.buildDefaultService();
		System.out.println("server starting");
		service.start();
	}

	public static void appiumConfig() {
		builder = new AppiumServiceBuilder();
		builder.withIPAddress("127.0.0.1");
		builder.usingPort(4723);
		builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
		builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");
		service = AppiumDriverLocalService.buildService(builder);
		service.start();
		System.out.println("starting appium server");
		System.out.println("Connected");
	}


	public static void startAppiumServer() throws IOException, InterruptedException {

		Xls_Reader.readPreReqMethod();
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			startAppiumServerSimple();
			DesiredCap.intializeAndroid();
		} else {
			startAppiumServerSimple();
			DesiredCap.initilizeiOS();
			System.out.println("appium server started");
		}

	}

	
	public static void main(String[] args) throws IOException, InterruptedException {

		stopAppiumServer();

	}
}