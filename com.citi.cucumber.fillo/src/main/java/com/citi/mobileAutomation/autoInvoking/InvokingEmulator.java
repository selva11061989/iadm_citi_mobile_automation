package com.citi.mobileAutomation.autoInvoking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;



public class InvokingEmulator extends Thread {
	
	/**
	 * @Author BABU
	 * @Date Dec 15 2019
	 */
	
	private static final Logger LOGGER = Logger.getLogger(InvokingEmulator.class);

	@Test 
	public static void startAvd() throws IOException, InterruptedException {
		try {
			Xls_Reader.readPreReqMethod();
			ProcessBuilder pbEmulator = new ProcessBuilder(Xls_Reader.AVD_Emulator_PATH, "-avd", Xls_Reader.AVD_NAME);
			ProcessBuilder pbADB = new ProcessBuilder(Xls_Reader.AVD_PATH,"devices");
			Process pcADB = pbADB.start();
			InputStream is = pcADB.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			String adb = new String();
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					line += br.readLine();
					adb = line;
				}
			}
			if (adb.toLowerCase().contains("emulator-5554")) {
				System.out.println("AVD Device Already exists");
				System.out.println("Device is connected");
				Thread.sleep(10000);
				System.out.println("Waiting for the system to load");
			} else {
				System.out.println("Device Not connected");
				System.out.println("AVD starting");
				@SuppressWarnings("unused")
				Process pcEmulator = pbEmulator.start();
				System.out.println("AVD started");
				Thread.sleep(10000);
				System.out.println("Waiting for the system Not to load");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error in Starting AVD" + e.getMessage());
		}
	}

//	@AfterTest
	public static void stopAvd() throws IOException, InterruptedException {
		try {
			Thread.sleep(2000);
			for(int PORT=5554;PORT<=5556;PORT +=2) {
			ProcessBuilder pbEmulator = new ProcessBuilder(Xls_Reader.AVD_PATH,
					"-s", "emulator-"+PORT, "emu", "kill");
			ProcessBuilder pbADB = new ProcessBuilder(Xls_Reader.AVD_PATH,
					"devices");
			Process pcADB = pbADB.start();
			InputStream is = pcADB.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			String adb = new String();
			while ((line = br.readLine()) != null) {
				if (line.length() > 0) {
					line += br.readLine();
					adb = line;
				}
			}

			if (adb.toLowerCase().contains("emulator-"+PORT)) {
				System.out.println("Device is connected");
				@SuppressWarnings("unused")
				Process pcEmulator = pbEmulator.start();
				System.out.println("Emulator closed successfully");
			} else {
				System.out.println("Device Not connected");
			}}
		} catch (Exception e) {
			LOGGER.error("Error in Stoping AVD" + e.getMessage());
		}
	}
	
	public static void stopSimulator() throws IOException
	{
		String kill[]= {"killall","Simulator"};
		Runtime.getRuntime().exec(kill);
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		startAvd();
	}
}