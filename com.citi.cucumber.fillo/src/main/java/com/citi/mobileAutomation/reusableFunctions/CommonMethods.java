package com.citi.mobileAutomation.reusableFunctions;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.citi.mobileAutomation.capabilities.DesiredCap;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class CommonMethods {

	public static AppiumDriver driver;

	public static WebDriverWait wait;

	// ==================New Resuable functions===================================
	/*
	 * To send text to any field This function needs two parameters Element locator
	 * i.e xpath and Text you want to pass If element isn't found,It will wait 10sec
	 * to find the element
	 */

	public static void sendKeysTextBox(By by, String text) throws IOException {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.visibilityOfElementLocated(by)));
		ele.sendKeys(text);

	}

	public static void sendKeysTextBoxMultipleSession(By by, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.sendKeys(text);
			ExtentReport.getResult();
		}

		else {
			System.out.println("Element is Absent");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
			int x = 5;
			x = x / 0;

		}
	}

	/*
	 * To send text to any field which already has some text This method will first
	 * clear the field, then it will send text This function needs two parameters
	 * Element locator i.e xpath and Text you want to pass If element isn't found,It
	 * will wait 10sec to find the element
	 */

	public static void clearAndSendKeys(By by, String text) throws IOException {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
		ele.clear();
		ele.sendKeys(text);
	}

	public static void clearAndSendKeysMultipleSession(By by, String text) throws Exception {
		Thread.sleep(5000);

		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.clear();
			ele.sendKeys(text);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Preesent");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
			int x = 5;
			x = x / 0;

		}
	}

	/*
	 * To click on any element This function needs one parameters Element locator
	 * i.e xpath If element isn't found,It will wait 10sec to find the element
	 */

	public static void clickMethod(By by) throws IOException {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.visibilityOfElementLocated(by)));
		ele.click();
	}

	public static void clickMethodMultipleSession(By by) throws Exception {
		Thread.sleep(5000);

		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.click();
			Thread.sleep(5000);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
			int x = 5;
			x = x / 0;
		}

	}

	/*
	 * To Check whether the element is present or not This function needs one
	 * parameters Element locator i.e xpath If element isn't found,It will wait
	 * 10sec to find the element
	 */

	public static void isElementPresent(By by) throws Exception {
		Thread.sleep(5000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();

		}

	}

	/*
	 * To get text from some element This function needs one parameter Element
	 * locator i.e xpath
	 */
	// ==============For IOS Device====================
	public static String getTextElementBy(By by) {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
		return ele.getAttribute("name");

	}

	/*
	 * first param ---> locator i.e xpath we can set below values according to your
	 * platform Second param --->IOS ----> name, value Second Param --- >Android -->
	 * text
	 * 
	 */
	// ==============For IOS and Android Device====================

	public static void getTextElementByMultipleSessioniOS(By by, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.getAttribute(text);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
		}

	}

	/*
	 * To get text from some element This function needs one parameter Element
	 * locator i.e xpath
	 */
	// ==============For Android Device====================
	public static String getTextElementByText(By by) {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
		return ele.getAttribute("text");

	}

	/*
	 * To assert This function needs Three parameters actual and testing texts first
	 * param --> locator i.e xpath Second param ---> your expected value we can set
	 * below values according to your platform Third param --->IOS ----> name, value
	 * Third Param --- >Android --> text
	 * 
	 */

	public static void myAssertion(By by, String expected, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			String actualvalue = ele.getAttribute(text);
			Assert.assertTrue(actualvalue.equalsIgnoreCase(expected));
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();

		}

	}

	/*
	 * To tap on device's screen at any location This function needs two parameters
	 * x and y ordinates
	 */

	public static void tapMethod(int x, int y) throws IOException, InterruptedException {
		TouchAction touchAction = new TouchAction(DesiredCap.driver);
		PointOption points = new PointOption();
		touchAction.tap(points.point(x, y)).perform();
		System.out.println("Tap performed");
		Thread.sleep(5000);
	}

	/*
	 * To tap on any element This function needs one parameter Element locator i.e
	 * xpath
	 */

	public static void tapByElement(By by) throws IOException, InterruptedException {
		try {
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			System.out.println("element located");
			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			ElementOption eleOP = new ElementOption();
			PointOption points = new PointOption();
			TapOptions tapOptions = new TapOptions().withElement(eleOP.withElement(ele));
			touchAction.tap(tapOptions).perform();
			System.out.println("Tap performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}

	}

	/*
	 * To LongPress on element This function needs one parameter Element locator i.e
	 * xpath
	 */

	public static void longPressOnElement(By by) throws IOException, InterruptedException {
		try {
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			System.out.println("element located");
			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			LongPressOptions longpress = new LongPressOptions();
			ElementOption eleOpt = new ElementOption();
			touchAction.longPress(longpress.withElement(eleOpt.withElement(ele))).perform();
			System.out.println("LONG PRESS performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}

	}

	/*
	 * To LongPress on element This function needs two parameters x and y ordinates
	 */

	public static void longPressOnElementCor(int x, int y) throws IOException, InterruptedException {
		try {

			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			LongPressOptions longpress = new LongPressOptions();
			ElementOption eleOpt = new ElementOption();
			PointOption point = new PointOption();
			touchAction.longPress(longpress.withPosition(point.withCoordinates(x, y))).perform();
			System.out.println("LONG PRESS performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}

	}

	/*
	 * To Scroll
	 */

	public static void scrollByJavaScriptExe() throws IOException, InterruptedException {

		PointOption points = new PointOption();
		TouchAction action = new TouchAction(DesiredCap.driver);
		action.press(points.point(501, 451)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
				.moveTo(points.point(501, 200)).release().perform();
		Thread.sleep(3000);
	}

	/*
	 * To Scroll in all directions This function needs four parameters x and y
	 * ordinates one integer value one character value i.e for up/down/left/right
	 */
	public static void scrollByJavaScriptExe(int x, int y, int coordinate, char c) throws IOException {
		PointOption points = new PointOption();
		TouchAction action = new TouchAction(DesiredCap.driver);

		switch (c) {
		case '-':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(x, y + c + coordinate)).release().perform();
			break;
		case '+':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(x, y + c + coordinate)).release().perform();
			break;
		case 'L':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(coordinate, y)).release().perform();
			break;
		case 'R':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(x + coordinate, y)).release().perform();
			break;
		default:
			System.out.println("Scroll cant perform on that direction");
			break;
		}

	}

	/*
	 * To Switch from Native view to web view
	 * 
	 */
	public static void Switch_Native_To_Hybrid() {

		Set<String> contextNames = DesiredCap.driver.getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName);
		}
		DesiredCap.driver.context(contextNames.toArray()[1].toString());

	}

	/*
	 * To Switch from web view to Native view
	 * 
	 */
	public static void Switch_Hybrid_To_Native() {

		Set<String> contextNames = DesiredCap.driver.getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName);
		}
		DesiredCap.driver.context(contextNames.toArray()[0].toString());

	}

	/*
	 * To take Screenshots
	 */
	public static void screenShots() throws IOException {
		TakesScreenshot shots = (TakesScreenshot) DesiredCap.driver;
		File src = shots.getScreenshotAs(OutputType.FILE);
		Date dt = new Date();
		File dest = new File(
				"C:\\Users\\HP\\eclipse-workspace\\BABU\\Myntra\\src\\main\\resource\\Screenshot\\babu.png");
		FileHandler.copy(src, dest);
	}

	/*
	 * To Check element is present not This function needs one parameter Element
	 * locator i.e xpath
	 */
	public static boolean elementNotExist(By by) throws InterruptedException {
		return DesiredCap.driver.findElements(by).size() > 0;

	}

	public static void VerifyingAccountsAndroid(String accNumber, String productName, String accNumber1,
			String productName1) {
		String getAcc = driver.findElementByXPath("//*[contains(@text,'" + accNumber + "')]").getAttribute("text");
		System.out.println("Get Acc :" + getAcc);
		if (getAcc.contains(productName)) {
			boolean b = getAcc.contains(accNumber1);
			System.out.println("Product name:" + b);
			boolean b1 = getAcc.contains(productName1);
			System.out.println("Display Account No:" + b1);
			System.out.println("Assertion Passed");
		} else {
			System.out.println("Not Found");

		}

	}

	public static void VerifyingAccountsClickAndroid(String accNumber){
		driver.findElementByXPath("//*[contains(@text,'" + accNumber + "')]").click();

	}
	
	public static void VerifyingAccountsClickiOS(String accNumber){
		driver.findElementByXPath("//*[contains(@name,'" + accNumber + "')]").click();

	}

	// ====================Old Reusable Functions================================

	public static boolean isExist(String Id) {
		try {
			DesiredCap.driver.findElement(By.id(Id)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isExistXpath(String Id) {
		try {
			DesiredCap.driver.findElement(By.xpath(Id)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isExistSend(String Id, String keysToSend) {
		try {
			DesiredCap.driver.findElement(By.id(Id)).sendKeys(keysToSend);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void checkForTheElementIsPresentInDOM(String className) {
		List<MobileElement> inputs;
		Long startTime = System.currentTimeMillis();
		do {
			inputs = (List<MobileElement>) DesiredCap.driver.findElements(MobileBy.className(className));
			if (!inputs.isEmpty()) {
				System.out.println("FOUND!");
				break;
			} else {
				System.out.println("NoT Found..!!");
			}
		} while ((System.currentTimeMillis() - startTime) / 1000 < 100); // 100
																			// sec
																			// wait
	}

	public static void clearForWaitByID(String value) throws InterruptedException {
		Thread.sleep(4000);
		DesiredCap.driver.findElement(By.id(value)).clear();
	}

	public static void clcikByID(String value) {
		try {
			DesiredCap.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			DesiredCap.driver.findElement(By.id(value)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clickByXpath(String value) {
		try {
			DesiredCap.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			DesiredCap.driver.findElement(By.xpath(value)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clickForWaitByXpath(String value) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(DesiredCap.driver, 60);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(value))).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Set<String> findDuplicates(List<String> listContainingDuplicates) {
		final Set<String> setToReturn = new HashSet<String>();
		final Set<String> set1 = new HashSet<String>();
		for (String yourInt : listContainingDuplicates) {
			if (!set1.add(yourInt)) {
				setToReturn.add(yourInt);
			}
		}
		return setToReturn;
	}

	public static boolean sortOrder(List<String> value) {
		String previous = "";
		value = new ArrayList<String>();
		for (final String current : value) {
			value.add(current);
			if (current.compareTo(previous) < 0)
				return false;
			previous = current;
		}
		return true;
	}

	public static void clickByClassName(String elements, int index) throws InterruptedException {
		List<MobileElement> elementClick = DesiredCap.driver.findElementsByClassName(elements);
		elementClick.get(index).click();
	}

	public static void scrolliOS(String text) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<Object, Object> scrollObject = new HashMap<>();
		scrollObject.put("predicateString", "value == '" + text + "'");
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scroll", scrollObject);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id(text)).click();

	}

}
