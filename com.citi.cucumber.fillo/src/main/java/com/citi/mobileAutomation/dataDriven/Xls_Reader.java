package com.citi.mobileAutomation.dataDriven;

import java.io.IOException;

import org.junit.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Xls_Reader {

	public static String deviceNameIOS = "";
	public static String platformNameIOS = "";
	public static String platformVersionIOS = "";
	public static String udidIOS = "";
	public static String bundleID = "";
	public static String APP_PATH = "";

	public static String deviceName = "";
	public static String platformName = "";
	public static String platformVersion = "";
	public static String appPackage = "";
	public static String appActivity = "";

	public static String platformToBeTested = "";

	public static String AVD_NAME = "";
	public static String AVD_PATH = "";
	public static String AVD_Emulator_PATH = "";
	public static String APK_PATH = "";

	public static String xlsPath = "";

	public static String currentPassword = "";
	public static String userID = "";
	public static String password = "";
	public static String OTP = "";
	public static String cus_Number = "";

	public static String reportPath = "";
	public static String screenshotPath = "";

	static Fillo fillo = new Fillo();
	static Connection connection;
	static Recordset recordset = null;
	
	public static String appPathFromCmd = "";
	
	
	public static void readReportPathFromMvn() throws FilloException {
	
		appPathFromCmd=System.getProperty("apk_PATH");
		
		reportPath=System.getProperty("report_Path");

		screenshotPath=System.getProperty("screen_Shot_Path");
	
	}

	public static void readReportPath() throws FilloException {
		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String strQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='Report_Location'";
			recordset = connection.executeQuery(strQuery);

			while (recordset.next()) {
				reportPath = recordset.getField("VALUES");
				System.out.println(reportPath);
				break;
			}

			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String screenQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='Screenshots_Location'";
			recordset = connection.executeQuery(screenQuery);

			while (recordset.next()) {
				screenshotPath = recordset.getField("VALUES");
				System.out.println(screenshotPath);
				break;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void readPreReqMethod() {

		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String cpassQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='CurrentPassword'";
			recordset = connection.executeQuery(cpassQuery);

			while (recordset.next()) {
				currentPassword = recordset.getField("VALUES");

			}

			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String userQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='UserID'";
			recordset = connection.executeQuery(userQuery);

			while (recordset.next()) {
				userID = recordset.getField("VALUES");

			}
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String passwordQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='Password'";
			recordset = connection.executeQuery(passwordQuery);

			while (recordset.next()) {
				password = recordset.getField("VALUES");

			}

			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String OTPQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='OTP'";
			recordset = connection.executeQuery(OTPQuery);

			while (recordset.next()) {
				OTP = recordset.getField("VALUES");

			}

			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String cus_NumberQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='CustomerNumber'";
			recordset = connection.executeQuery(cus_NumberQuery);

			while (recordset.next()) {
				cus_Number = recordset.getField("VALUES");

			}
			recordset.close();
			connection.close();

		} catch (Exception e) {

		}
		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String strQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformToBeTested'";
			recordset = connection.executeQuery(strQuery);

			while (recordset.next()) {
				platformToBeTested = recordset.getField("VALUES");

			}

			if (platformToBeTested.equalsIgnoreCase("Android")) {

				String deviceQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='deviceName'";
				recordset = connection.executeQuery(deviceQuery);
				while (recordset.next()) {

					deviceName = recordset.getField("values");

				}

				String platformQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformName'";
				recordset = connection.executeQuery(platformQuery);
				while (recordset.next()) {

					platformName = recordset.getField("values");

				}

				String platformVersionQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformVersion'";
				recordset = connection.executeQuery(platformVersionQuery);
				while (recordset.next()) {

					platformVersion = recordset.getField("values");

				}

				String actQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='appActivity'";
				recordset = connection.executeQuery(actQuery);
				while (recordset.next()) {
					System.out.println(recordset.getField("values"));
					appActivity = recordset.getField("values");

				}
				String packQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='appPackage '";
				recordset = connection.executeQuery(packQuery);
				while (recordset.next()) {

					appPackage = recordset.getField("values");

				}

				String avdNameQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='AVD_NAME'";
				recordset = connection.executeQuery(avdNameQuery);
				while (recordset.next()) {

					AVD_NAME = recordset.getField("values");

				}
				String avdPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='AVD_PATH'";
				recordset = connection.executeQuery(avdPathQuery);
				while (recordset.next()) {

					AVD_PATH = recordset.getField("values");
				}

				String avdEmuPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='AVD_Emulator_PATH'";
				recordset = connection.executeQuery(avdEmuPathQuery);
				while (recordset.next()) {

					AVD_Emulator_PATH = recordset.getField("values");
				}
				
				String apkPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='APK_PATH'";
				recordset = connection.executeQuery(apkPathQuery);
				while (recordset.next()) {

					APK_PATH = recordset.getField("values");
				}

				recordset.close();
				connection.close();
			}

			else {

				String deviceQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='deviceNameIOS'";
				recordset = connection.executeQuery(deviceQuery);
				while (recordset.next()) {

					deviceNameIOS = recordset.getField("values");

				}

				String platformQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformNameIOS'";
				recordset = connection.executeQuery(platformQuery);
				while (recordset.next()) {

					platformNameIOS = recordset.getField("values");

				}

				String platformVersionQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformVersionIOS'";
				recordset = connection.executeQuery(platformVersionQuery);
				while (recordset.next()) {

					platformVersionIOS = recordset.getField("values");

				}

				String actQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='udidIOS'";
				recordset = connection.executeQuery(actQuery);
				while (recordset.next()) {

					udidIOS = recordset.getField("values");

				}
				String packQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='bundleID'";
				recordset = connection.executeQuery(packQuery);
				while (recordset.next()) {

					bundleID = recordset.getField("values");

				}
				String appPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='APP_PATH'";
				recordset = connection.executeQuery(appPathQuery);
				while (recordset.next()) {

					APP_PATH = recordset.getField("values");

				}
				recordset.close();
				connection.close();
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void main(String[] args) throws IOException, FilloException {
		readPreReqMethod();
	}

}