package com.citi.mobileAutomation.reportGeneration;

import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

public class Base64Class {
		    public static String encodeFileToBase64Binary(String path){
		    	File file =  new File(path);
		         String encodedfile = null;
		         try {
		             FileInputStream fileInputStreamReader = new FileInputStream(file);
		             byte[] bytes = new byte[(int)file.length()];
		             fileInputStreamReader.read(bytes);
		             encodedfile = Base64.getEncoder().encodeToString(bytes);
		             
		         } catch (Exception e) {
		             // TODO Auto-generated catch block
		             e.printStackTrace();
		         }
		         return "data:image/jpg;base64, " + encodedfile ;
		     }


}
