package com.citi.mobileAutomation.reportGeneration;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReportClassWithSnapshot {

	public static ExtentReports extent;
	public static ExtentTest logger;
	public static WebDriver driver;
	
	
	@BeforeTest
	public static void startReport(){
		System.setProperty("webdriver.chrome.driver","/Users/apple/Downloads/chromedriver");
		driver = new ChromeDriver();
		System.out.println(System.getProperty("user.dir"));
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/CustomReport.html", false);
		//extent.addSystemInfo("Environment","Environment Name")
		extent
		.addSystemInfo("Host Name", "Maveric-Orion")
		.addSystemInfo("Environment", "Automation Testing")
		.addSystemInfo("User Name", "Babu");

		//extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}


	public static String getScreenshot(WebDriver driver, String screenshotName) throws Exception {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		String destination = System.getProperty("user.dir") + "/ScreenShots/"+screenshotName+dateName+".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}

	@Test
	public void passTest(){

		logger = extent.startTest("BabuPassTest");
		Assert.assertTrue(true);
		driver.get("https://www.amazon.com");
		logger.log(LogStatus.PASS, "Test Case Passed is passTest");
	}

	@Test
	public void failTest()
	{
		logger = extent.startTest("failTest");
		driver.get("https://www.softwaretestingmaterial.com");
		String currentURL = driver.getCurrentUrl();
		Assert.assertEquals(currentURL, "NoTitle");
		logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
	}

	@Test
	public void skipTest(){
		logger = extent.startTest("skipTest");
		throw new SkipException("Skipping - This is not ready for testing ");
	}

	@AfterMethod
	public static void getResult(ITestResult result) throws Exception{
		if(result.getStatus() == ITestResult.FAILURE){
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
			String screenshotPath = ExtentReportClassWithSnapshot.getScreenshot(driver, result.getName());

			logger.log(LogStatus.FAIL, logger.addScreenCapture(screenshotPath));
		}
		else if(result.getStatus() == ITestResult.SKIP){
			logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
			//logger.log(LogStatus.SKIP, logger.addScreenCapture(screenshotPath));
		}
		
		else if(result.getStatus()==ITestResult.SUCCESS)
		{
			logger.log(LogStatus.PASS, "Test Case Passed is "+result.getName());
			String screenshotPath = ExtentReportClassWithSnapshot.getScreenshot(driver, result.getName());

			logger.log(LogStatus.PASS, logger.addScreenCapture(screenshotPath));
		}

		extent.endTest(logger);
	}
	@AfterTest
	public void endReport(){

		extent.flush();
		extent.close();
	}
	
	
}



