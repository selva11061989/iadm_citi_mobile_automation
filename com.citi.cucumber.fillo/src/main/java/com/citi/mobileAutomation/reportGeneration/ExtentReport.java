package com.citi.mobileAutomation.reportGeneration;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.citi.mobileAutomation.capabilities.DesiredCap;
import com.citi.mobileAutomation.dataDriven.CommomReadFunction;
import com.citi.mobileAutomation.dataDriven.DirectoryForReportAndScreenshots;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;

import cucumber.api.Scenario;
import cucumber.api.java.Before;


public class ExtentReport {
	
	public static WebDriver driver;

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent2;
	public static ExtentTest test;

	// public static Scenario scene=null;
	public static String scenarioName = null;
	
	public static String stepName = null;

	public static String[] tagName;

	public static String status = "pass";
	
	public static int count = 0;

	static Fillo fillo = new Fillo();
	static Connection connection;

	
	public static void getScenarioName(Scenario scene) throws FilloException {
		scenarioName = scene.getName();
		System.out.println(scenarioName);
		ArrayList arr = new ArrayList(scene.getSourceTagNames());
		String tags;
		tags = arr.get(0).toString();
		tagName = tags.split("@");
		// System.out.println(tagName[1]);
		
		if (count == 0) {	
		ExtentReport.extentReportSetup();
		count++;
		//extent2.flush();
		
	}
		
		test = extent2.createTest(scenarioName);
		
	}
	
	public static void extentReportSetup() throws FilloException {

		DirectoryForReportAndScreenshots.createDirectory();
		String dateName = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss").format(new Date());
		htmlReporter = new ExtentHtmlReporter(Xls_Reader.reportPath+ "CustomReport" + dateName + ".html");
		extent2 = new ExtentReports();
		extent2.attachReporter(htmlReporter);

		htmlReporter.config().setDocumentTitle("Automation Report by Babu");
		htmlReporter.config().setReportName("Custom Report ");
		htmlReporter.config().setTheme(Theme.DARK);

		extent2.setSystemInfo("Application Name", "CITI");
		extent2.setSystemInfo("User Name", "Maveric");
		extent2.setSystemInfo("Envirnoment", "Production");
		System.out.println("Inside Extent");
	}
	
	public static void updateExcelStatus(Scenario scenario) throws FilloException {
		if (scenario.isFailed()) {
			connection = fillo.getConnection(
					"" + CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX") + "");
			String strQuery = "Update Scenarios Set Status='Failed' where Scenario='" + tagName[1] + "'";

			connection.executeUpdate(strQuery);
			connection.close();
		} 
			
			else {
				connection = fillo.getConnection(
						"" + CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX") + "");
				String strQuery = "Update Scenarios Set Status='Passed' where Scenario='" + tagName[1] + "'";

				connection.executeUpdate(strQuery);
				connection.close();
				
			}
			
		}
	
	
//	public static void getResult() throws Exception {
//
//		if (ExtentReport.status.equals("fail")) {
//			test = extent2.createTest("" + ExtentReport.stepName + "");
//			test.log(Status.FAIL, MarkupHelper.createLabel(" - Test Case Failed", ExtentColor.RED));
//			String screenshotPath = ScreenShotClass.getScreenshot(DesiredCap.driver, "Failed Test Case");
//			test.fail("Test Case Failed Snapshot is below " + test.addScreenCaptureFromPath(screenshotPath));
//
//		} else if (ExtentReport.status.equals("skip")) {
//			test = extent2.createTest("" + ExtentReport.stepName + "");
//			test.log(Status.SKIP, MarkupHelper.createLabel(" - Test Case Skipped", ExtentColor.ORANGE));
//		}
//
//		else if (ExtentReport.status.equals("pass")) {
//			test = extent2.createTest("" + ExtentReport.stepName + "");
//			test.log(Status.PASS, MarkupHelper.createLabel(" Test Case PASSED", ExtentColor.GREEN));
//
//			String screenshotPath = ScreenShotClass.getScreenshot(DesiredCap.driver, "Pass Test Case");
//			test.pass("Test Case Passed Snapshot is below " + test.addScreenCaptureFromPath(screenshotPath));
//		}
//		extent2.flush();
//		System.out.println("report  tag----");
//
//	}
	
	public static void getResult() throws Exception {

		if (ExtentReport.status.equals("fail")) {

		String screenshotPath = ScreenShotClass.getScreenshot(DesiredCap.driver, "Failed Test Step");

		test.createNode(ExtentReport.stepName).addScreenCaptureFromBase64String(screenshotPath).fail("Test Step Failed");



		} else if (ExtentReport.status.equals("skip")) {

		String screenshotPath = ScreenShotClass.getScreenshot(DesiredCap.driver, "Failed Test Step");

		test.createNode(ExtentReport.stepName).addScreenCaptureFromBase64String(screenshotPath).skip("Test Step Skipped");


		}


		else if (ExtentReport.status.equals("pass")) {

		String screenshotPath = ScreenShotClass.getScreenshot(DesiredCap.driver, "Pass Test Step");

		test.createNode(ExtentReport.stepName).addScreenCaptureFromBase64String(screenshotPath).pass("Test Step Passed");

		}

		extent2.flush();


		}


}