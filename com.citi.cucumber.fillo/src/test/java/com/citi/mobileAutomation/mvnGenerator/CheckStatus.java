package com.citi.mobileAutomation.mvnGenerator;

import java.util.ArrayList;

import org.junit.Before;

import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.citi.mobileAutomation.dataDriven.*;

import io.cucumber.testng.AbstractTestNGCucumberTests;

public class CheckStatus extends AbstractTestNGCucumberTests {
	static Fillo fillo = new Fillo();
	static Connection connection;
	static Recordset recordset = null;
	static String status = null;
	public static String scenarioName = null;
	public static ArrayList<String> arr = new ArrayList<String>();
	public static String runFile = "";
	public static String dir = "";

	int i = 1;

	public static String first = "mvn test -Ddynamic.runner.tag.add=@";
	//public static String second = " mvn test -Ddynamic.runner.tag.add=@";

	public static String platformName = "";

	@Before
	public static void main(String[] args) {
		dir = System.getProperty("user.dir");
		System.out.println(dir);
		readStatusMethod();
	}

	public static void readStatusMethod() {
		try {
			dir = System.getProperty("user.dir");
			connection = fillo.getConnection(
					"" + CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX") + "");
			String strQuery = "Select * from PRE_REQ_FOR_TEST where parameters='status'";
			recordset = connection.executeQuery(strQuery);

			while (recordset.next()) {
				System.out.println(recordset.getField("values"));

				status = recordset.getField("values");
			}

			recordset.close();
			connection.close();

			readScenarioMethod();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void readScenarioMethod() {
		try {

			connection = fillo.getConnection(
					"" + CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX") + "");
			String strQuery = "Select * from Scenarios where Status='" + status + "'";
			recordset = connection.executeQuery(strQuery);

			while (recordset.next()) {
				try {
					first = "mvn test -Ddynamic.runner.tag.add=@";
					first = first + recordset.getField("Scenario");
					arr.add(first);
					//runFile = runFile + first;
				}

				catch (Exception e) {
					System.out.println("HEY Buddy ! U r Doing Something Wrong ");
					e.printStackTrace();
				}

			}
			System.out.println(runFile);

			recordset.close();
			connection.close();
			System.out.println(arr);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void WriteMethod() {
		try {

			connection = fillo.getConnection(
					"" + CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX") + "");
			String strQuery = "Update Scenarios Set Status='Pass' where Scenario='" + recordset.getField("Scenario")
					+ "'";

			connection.executeUpdate(strQuery);
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
