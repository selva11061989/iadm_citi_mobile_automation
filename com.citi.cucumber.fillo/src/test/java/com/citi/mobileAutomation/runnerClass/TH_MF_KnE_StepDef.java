package com.citi.mobileAutomation.runnerClass;

import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.objectRepository.TH_MF_KnE_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TH_MF_KnE_StepDef {
	
	@Given("^login with mrc customer for Wealth Dashboard$")
	public void login_with_mrc_customer_for_Wealth_Dashboard() throws Throwable {
		OpeningAppiumServer.startAppiumServer();
		ExtentReport.stepName = "Login with mrc customer";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.continue_Android);
		ExtentReport.stepName = "Click the respective language";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.langBtn);
		ExtentReport.stepName = "Click accept button";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.acceptBtn);
		ExtentReport.stepName = "Click log into existing";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.existingLoginBtn);
		ExtentReport.stepName = "Enter the user id";
		CommonMethods.sendKeysTextBox(TH_MF_KnE_Objects.username_Android, Xls_Reader.userID);
		ExtentReport.stepName = "Enter the password";
		CommonMethods.sendKeysTextBox(TH_MF_KnE_Objects.password_Android, Xls_Reader.password);
		ExtentReport.stepName = "Click the login button";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.loginButton_Android);
		CommonMethods.clickMethod(TH_MF_KnE_Objects.wealth_Android);
		CommonMethods.clickMethod(TH_MF_KnE_Objects.bmfChevron);
	}

	@When("^To verify whether the application is eMutualFundSearch(\\d+) Close X getting navigated to the required screen Search entry point on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_eMutualFundSearch_Close_X_getting_navigated_to_the_required_screen_Search_entry_point_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is eMutualFundSearch Close X getting navigated to the required screen Search entry point on clicking th provided image or Button";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.closeBtn);
		ExtentReport.stepName = "To verify whether the application is eMutualFundSearch Close X is present";
		CommonMethods.clickMethodMultipleSession(TH_MF_KnE_Objects.bmfChevron);
		
	}

	@Then("^To Verify whether the required mandatory Field Error Icon is getting displayed as per the condition Always$")
	public void to_Verify_whether_the_required_mandatory_Field_Error_Icon_is_getting_displayed_as_per_the_condition_Always() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Error Icon is getting displayed as per the condition Always";
		CommonMethods.isElementPresent(TH_MF_KnE_Objects.errorIcon);
	}

	@And("^To Verify whether the required mandatory Field Complete your investment profile is getting displayed as per the condition Always$")
	public void to_Verify_whether_the_required_mandatory_Field_Complete_your_investment_profile_is_getting_displayed_as_per_the_condition_Always() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Complete your investment profile is getting displayed as per the condition Always";
		CommonMethods.isElementPresent(TH_MF_KnE_Objects.completeYourInvProf);
	}

	@And("^To Verify whether the required mandatory Field Take a few minutes to update your investement profile before proceeding is getting displayed as per the condition Always$")
	public void to_Verify_whether_the_required_mandatory_Field_Take_a_few_minutes_to_update_your_investement_profile_before_proceeding_is_getting_displayed_as_per_the_condition_Always() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Take a few minutes to update your investement profile before proceeding is getting displayed as per the condition Always";
		CommonMethods.isElementPresent(TH_MF_KnE_Objects.content);
	}

	@And("^To Verify whether the required mandatory Field Complete my profile is getting displayed as per the condition Always$")
	public void to_Verify_whether_the_required_mandatory_Field_Complete_my_profile_is_getting_displayed_as_per_the_condition_Always() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Complete my profile is getting displayed as per the condition Always";
		CommonMethods.isElementPresent(TH_MF_KnE_Objects.completeMyProfBtn);
	}
	

}
