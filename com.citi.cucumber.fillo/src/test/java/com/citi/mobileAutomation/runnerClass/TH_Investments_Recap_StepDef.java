package com.citi.mobileAutomation.runnerClass;

import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.objectRepository.TH_Investments_Recap_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TH_Investments_Recap_StepDef {
	
	@Given("^Login with mrc customer Recap$")
	public void login_with_mrc_customer_Recap() throws Throwable {
		
		OpeningAppiumServer.startAppiumServer();
		ExtentReport.stepName = "Login with mrc customer";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.continue_Android);
		ExtentReport.stepName = "Click the respective language";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.langBtn);
		ExtentReport.stepName = "Click accept button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.acceptBtn);
		CommonMethods.clickMethod(TH_Investments_Recap_Objects.existingLoginBtn);
		ExtentReport.stepName = "Enter the user id";
		CommonMethods.sendKeysTextBox(TH_Investments_Recap_Objects.username_Android, Xls_Reader.userID);
		ExtentReport.stepName = "Enter the password";
		CommonMethods.sendKeysTextBox(TH_Investments_Recap_Objects.password_Android, Xls_Reader.password);
		ExtentReport.stepName = "Click the login button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.loginButton_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.getStarted_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.enableButton_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.alert_Ok_Btn_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.experienceCitiMobile_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.experienceCitiMobile_Android);
		Thread.sleep(9000);
		ExtentReport.stepName = "Click on investment down chevron";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.investmentsdownChevron);
		ExtentReport.stepName = "Click on mutual fund chevron";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.mutualFundChevron);
		ExtentReport.stepName = "Click on holdings chevron";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.holdingsChevron);
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
		ExtentReport.stepName = "Click on an account";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.acctSelection);
		ExtentReport.stepName = "Click on sell button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.ctaSellBtn);
		//CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.sellBtn);
		Thread.sleep(10000);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.description);
		Thread.sleep(9000);
		ExtentReport.stepName = "Input the amount";
		CommonMethods.sendKeysTextBoxMultipleSession(TH_Investments_Recap_Objects.inputAmt, "100000000");
		Thread.sleep(9000);
		ExtentReport.stepName = "Click the forward button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.forwardBtn);
		Thread.sleep(9000);
	}

	@And("^To verify whether the application is Recap Mutual Fund Sell Back getting navigated to the required screen eMutualFundSell (\\d+) on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Back_getting_navigated_to_the_required_screen_eMutualFundSell_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is Recap Mutual Fund Sell Back getting navigated to the required screen eMutualFundSell (\\d+) on clicking th provided image or Button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.backBtn);
		Thread.sleep(10000);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.forwardBtn);
	}

	@And("^To Verify whether the required mandatory Field Review and confirm is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_Review_and_confirm_is_getting_displayed_as_per_the_condition() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Review and confirm is getting displayed as per the condition";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.reviewInvestment);
	}

	@And("^To verify whether the application is Recap Mutual Fund Sell Close X getting navigated to the required screen Dashboard on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Close_X_getting_navigated_to_the_required_screen_Dashboard_on_clicking_th_provided_image_or_Button() throws Throwable {
		ExtentReport.stepName = "To verify whether the application is Recap Mutual Fund Sell Close X getting navigated to the required screen Dashboard on clicking th provided image or Button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.closeBtn);
		Thread.sleep(10000);
		//CommonMethods.isElementPresent(TH_Investments_Recap_Objects.sellBtn);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.ctaSellBtn);
		CommonMethods.sendKeysTextBoxMultipleSession(TH_Investments_Recap_Objects.inputAmt, "100000000");
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.secondForward);
		Thread.sleep(10000);
		
	}

	@Then("^To verify whether the You have a sell order of fundName is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_You_have_a_sell_order_of_fundName_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the You have a sell order of fundName is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.sellOrderHeader);
		Thread.sleep(10000);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.acctName);
		Thread.sleep(10000);
	}

	@And("^To verify whether the You have redeemed fundUnitsRedeemed units is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_You_have_redeemed_fundUnitsRedeemed_units_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the You have redeemed fundUnitsRedeemed units is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.redeemTxt);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.units);
	}

	@Then("^To verify whether the Order date and sellOrderDate and Trade will be completed within (\\d+) working days is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Order_date_and_sellOrderDate_and_Trade_will_be_completed_within_working_days_is_displayed_details_are_matching_with_the_API_Responses_API_Field(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the Order date and sellOrderDate and Trade will be completed within (\\d+) working days is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.orderDate);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.date);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.tradeDisclaimer);
	}
	
	@Then("^To verify whether the Credit to account number settlementAccountName and settlementAccountNumber is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Credit_to_account_number_settlementAccountName_and_settlementAccountNumber_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Credit to account number settlementAccountName and settlementAccountNumber is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.creditToAcctNum);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.creditNum);
	}
	
	@And("^To verify whether the application is Recap Mutual Fund Sell Credit to account number settlementAccountName and settlementAccountNumber getting navigated to the required screen Display Change Settlement Account block on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Credit_to_account_number_settlementAccountName_and_settlementAccountNumber_getting_navigated_to_the_required_screen_Display_Change_Settlement_Account_block_on_clicking_th_provided_image_or_Button() throws Throwable {
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.creditToAcctNum);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.creditNum);
		Thread.sleep(9000);
	}

	@And("^To verify whether the settlementAccountName and settlementAccountNumber is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_settlementAccountName_and_settlementAccountNumber_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.acctHeader);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.acctNum);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.closeBtnsecond);
		Thread.sleep(9000);
	}

//	@Then("^To Verify whether the required mandatory Field Trade will be placed the next business day and Please note that the transaction will be for the next business days trade as it is currently past the funds transaction cutoff time is getting displayed as per the condition$")
//	public void to_Verify_whether_the_required_mandatory_Field_Trade_will_be_placed_the_next_business_day_and_Please_note_that_the_transaction_will_be_for_the_next_business_days_trade_as_it_is_currently_past_the_funds_transaction_cutoff_time_is_getting_displayed_as_per_the_condition() throws Throwable {
//		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.loginButton_Android);
//		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.creditToAcctNum);
//	}
//
//	@And("^To Verify whether the required mandatory Field Proceed is getting displayed as per the condition$")
//	public void to_Verify_whether_the_required_mandatory_Field_Proceed_is_getting_displayed_as_per_the_condition() throws Throwable {
//	}
//
//	@Then("^To verify whether the application is Recap Mutual Fund Sell Proceed getting navigated to the required screen eMutualFundSell (\\d+) on clicking th provided image or Button$")
//	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Proceed_getting_navigated_to_the_required_screen_eMutualFundSell_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
//		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.loginButton_Android);
//	}
//
//	@And("^To Verify whether the required mandatory Field Cancel is getting displayed as per the condition$")
//	public void to_Verify_whether_the_required_mandatory_Field_Cancel_is_getting_displayed_as_per_the_condition() throws Throwable {
//		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.loginButton_Android);
//	}

//	@Then("^To verify whether the application is Recap Mutual Fund Sell Cancel getting navigated to the required screen eMutualFundSell (\\d+) on clicking th provided image or Button$")
//	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Cancel_getting_navigated_to_the_required_screen_eMutualFundSell_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
//		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.loginButton_Android);
//	}

	@And("^To verify whether the Amount credited S\\$ and sellAmountCredited The amount shown includes a charge of (\\d+) redemption fee which is applicabe to this mutual fund is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Amount_credited_S$_and_sellAmountCredited_The_amount_shown_includes_a_charge_of_redemption_fee_which_is_applicabe_to_this_mutual_fund_is_displayed_details_are_matching_with_the_API_Responses_API_Field(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the Amount credited S\\$ and sellAmountCredited The amount shown includes a charge of (\\d+) redemption fee which is applicabe to this mutual fund is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.amountCredited);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.amountVal);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.amtDescription);
	}

	@Then("^To verify whether the Fund Code and fundCode is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Fund_Code_and_fundCode_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Fund Code and fundCode is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.fundCode);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.acash);
	}

	@And("^To verify whether the Risk rating and fundRiskRating is displayed details are matching with the API Responses: API Field$")
	public void to_verify_whether_the_Risk_rating_and_fundRiskRating_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Risk rating and fundRiskRating is displayed details are matching with the API Responses: API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.riskRating);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.riskRatingVal);
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
	}

	@Then("^To verify whether the Available units and fundAvailableUnits is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Available_units_and_fundAvailableUnits_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Available units and fundAvailableUnits is displayed details are matching with the API Responses API Field";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.availableUnits);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.availableUnitsVal);
	}

	@And("^To verify whether the Indicative Nav fundIndicativeNAV as of navLastUpdateDate is displayed details are matching with the API Responses APIField$")
	public void to_verify_whether_the_Indicative_Nav_fundIndicativeNAV_as_of_navLastUpdateDate_is_displayed_details_are_matching_with_the_API_Responses_APIField() throws Throwable {
		ExtentReport.stepName = "To verify whether the Indicative Nav fundIndicativeNAV as of navLastUpdateDate is displayed details are matching with the API Responses APIField";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.indicativeNavRecap);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.indicativeNavVal);
	}

	@Then("^To verify whether the Fund currency type and fundCurrencyType is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Fund_currency_type_and_fundCurrencyType_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Fund currency type and fundCurrencyType is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.fundCurrencyType);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.fundCurrencyVal);
		CommonMethods.scrollByJavaScriptExe();
		Thread.sleep(5000);
		CommonMethods.scrollByJavaScriptExe();
		Thread.sleep(5000);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.closeBtnsecond);
		CommonMethods.scrollByJavaScriptExe();
	}

	@And("^To Verify whether the required mandatory Field Transaction type Non advised is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_Transaction_type_Non_advised_is_getting_displayed_as_per_the_condition() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Transaction type Non advised is getting displayed as per the condition";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.transactionType);
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.transactionTypeVal);
		CommonMethods.scrollByJavaScriptExe();
	}

	@Then("^To Verify whether the required mandatory Field By tapping Confirm you acknowledge and agree with Citi Singapore Limited to is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_By_tapping_Confirm_you_acknowledge_and_agree_with_Citi_Singapore_Limited_to_is_getting_displayed_as_per_the_condition() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field By tapping Confirm you acknowledge and agree with Citi Singapore Limited to is getting displayed as per the condition";
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.disclaimer);
	}

	@And("^To Verify whether the required mandatory Field PDF Icon is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_PDF_Icon_is_getting_displayed_as_per_the_condition() throws Throwable {
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.pdf1);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.backBtnLast);
		Thread.sleep(5000);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.pdf2);
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.backBtnLast);
		Thread.sleep(5000);
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
	}

	@Then("^To verify whether the application is Recap Mutual Fund Sell Terms and Conditions getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Terms_and_Conditions_getting_navigated_to_the_required_screen_Open_corresponding_PDF_on_clicking_th_provided_image_or_Button() throws Throwable {
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.termsAndConditions);
		Thread.sleep(9000);
	}

	@And("^To verify whether the application is Recap Mutual Fund Sell Disclosure St getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Disclosure_St_getting_navigated_to_the_required_screen_Open_corresponding_PDF_on_clicking_th_provided_image_or_Button() throws Throwable {
		CommonMethods.isElementPresent(TH_Investments_Recap_Objects.disclosureStatement);
	}

	@Then("^To verify whether the application is Recap Mutual Fund Sell Confirm getting navigated to the required screen eMutualFundSell (\\d+) Display Cut-off time exceeded block on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Recap_Mutual_Fund_Sell_Confirm_getting_navigated_to_the_required_screen_eMutualFundSell_Display_Cut_off_time_exceeded_block_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is Recap Mutual Fund Sell Confirm getting navigated to the required screen eMutualFundSell (\\d+) Display Cut-off time exceeded block on clicking th provided image or Button";
		CommonMethods.clickMethodMultipleSession(TH_Investments_Recap_Objects.confirmBtn);
	}
	
	
}
