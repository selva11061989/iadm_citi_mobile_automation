package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.capabilities.DesiredCap;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.objectRepository.A_ReferenceObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class A_ReferenceStepdef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}

	/*
	 * @Author:Selva, 
	 * Product:Prerequiste_Validation, 
	 * Platform:Android_iOS
	 * 
	 */

	@Given("^Click on Continue Button$")
	public void click_on_Continue_Button() throws Throwable {

		OpeningAppiumServer.startAppiumServer();

		ExtentReport.stepName = "Click on Continue Button";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.continue_And);
		} else {
			
			CommonMethods.tapMethod(254, 407);
		}

	}

	@When("^Enter on Login Page$")
	public void enter_on_Login_Page() throws Throwable {

		ExtentReport.stepName = "Enter on Login Page";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.myAssertion(A_ReferenceObjects.activateCard_And, "Activate card", "text");

		} else {

			CommonMethods.myAssertion(A_ReferenceObjects.activateCard_iOS, "Activate card", "name");

		}

	}

	@Then("^To Verify the Activate Card is Displayed or Not$")
	public void to_Verify_the_Activate_Card_is_Displayed_or_Not() throws Throwable {

		ExtentReport.stepName = "To Verify the Activate Card is Displayed or Not";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.isElementPresent(A_ReferenceObjects.activateCard_And);

		} else {

			CommonMethods.isElementPresent(A_ReferenceObjects.activateCard_iOS);
		}

	}

	@Then("^To Verify the Create a Login Account is Displayed or Not$")
	public void to_Verify_the_Create_a_Login_Account_is_Displayed_or_Not() throws Throwable {

		ExtentReport.stepName = "To Verify the Create a Login Account is Displayed or Not";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.myAssertion(A_ReferenceObjects.loginAccount_And, "New? Create a login account", "text");

		} else {

			CommonMethods.myAssertion(A_ReferenceObjects.loginAccount_iOS, "New? Create a login account", "name");

		}

	}

	@Then("^To Verify the Create a Login Account Button Element is wrong$")
	public void to_Verify_the_Create_a_Login_Account_Button_Element_is_wrong() throws Throwable {

		ExtentReport.stepName = "To Verify the Create a Login Account Button Element is wrong";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.myAssertion(A_ReferenceObjects.loginAccountWrong_And, "New? Create a login account", "text");
		} else {

			CommonMethods.myAssertion(A_ReferenceObjects.loginAccountWrong_iOS, "New? Create a login account", "name");

		}

	}

	@Then("^To Verify the Login with Existing Account is Displayed or Not$")
	public void to_Verify_the_Login_with_Existing_Account_is_Displayed_or_Not() throws Throwable {

		ExtentReport.stepName = "To Verify the Login with Existing Account is Displayed or Not";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.myAssertion(A_ReferenceObjects.existingAcc_And, "Log in with existing account", "text");

		} else {

			CommonMethods.myAssertion(A_ReferenceObjects.existingAcc_iOS, "Log in with existing account", "name");

		}

	}

	@When("^Click on Login with Existing Account$")
	public void click_on_Login_with_Existing_Account() throws Throwable {

		ExtentReport.stepName = "Click on Login with Existing Account";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.existingAcc_And);

		} else {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.existingAcc_iOS);

		}

	}

	@When("^Enter Your Username and Password$")
	public void enter_Your_Username_and_Password() throws Throwable {

		ExtentReport.stepName = "Enter Your Username and Password";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.userID_And);
			CommonMethods.sendKeysTextBoxMultipleSession(A_ReferenceObjects.userID_And, "sg_citi_1234");
			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.password_And);
			CommonMethods.sendKeysTextBoxMultipleSession(A_ReferenceObjects.password_And, "Sdet2020");

		} else {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.userID_iOS);
			CommonMethods.sendKeysTextBoxMultipleSession(A_ReferenceObjects.userID_iOS, "sg_citi_1234");
			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.password_iOS);
			CommonMethods.sendKeysTextBoxMultipleSession(A_ReferenceObjects.password_iOS, "Sdet2020");
		}

	}

	@When("^click on Login Button$")
	public void click_on_Login_Button() throws Throwable {

		ExtentReport.stepName = "click on Login Button";

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.login_And);

		} else {

			CommonMethods.clickMethodMultipleSession(A_ReferenceObjects.login_iOS);

		}

	}

	// To stop server and emulator/simulator

	@Given("^user click on logout button$")
	public void user_click_on_logout_button() throws Throwable {

		OpeningAppiumServer.stopAppiumServer();

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			InvokingEmulator.stopAvd();
		} else {
			InvokingEmulator.stopSimulator();
		}

	}

	@After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}

// ==================================================================================================

// ExtentReport.extentReportSetup();
// ExtentReport.test =
// ExtentReport.extent2.createTest(ExtentReport.scenarioName);
// ExtentReport.extent2.flush();
// ExtentReport.count++;
