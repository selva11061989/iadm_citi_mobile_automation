package com.citi.mobileAutomation.runnerClass;

import java.io.IOException;

import org.junit.runner.RunWith;

import com.citi.mobileAutomation.cucumberClass.CustomCucumber;
import com.citi.mobileAutomation.mvnGenerator.CheckStatus;

import cucumber.api.CucumberOptions;

@RunWith(CustomCucumber.class)
@CucumberOptions(
		//tags = { "@TWA1" }, 
		dryRun = false,
	 //glue = {"com.citi.cucumber.fillo/src/test/java/com/citi/mobileAutomation/Step_Definition"},
		features = {"Resources/Sprint-20.07" }

)
public class RunnerFile {

}
