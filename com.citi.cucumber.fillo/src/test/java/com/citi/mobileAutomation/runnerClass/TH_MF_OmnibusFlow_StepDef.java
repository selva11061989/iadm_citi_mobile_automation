package com.citi.mobileAutomation.runnerClass;

import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.objectRepository.TH_MF_OmnibusFlow_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class TH_MF_OmnibusFlow_StepDef {
	
	@Given("^Login with MRC customer for account opening$")
	public void login_with_MRC_customer_for_account_opening() throws Throwable {
		OpeningAppiumServer.startAppiumServer();
		ExtentReport.stepName = "Login with mrc customer";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.continue_Android);
		ExtentReport.stepName = "Enter the password";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.enter_password_Android);
		CommonMethods.sendKeysTextBox(TH_MF_OmnibusFlow_Objects.password_Android, Xls_Reader.password);
		ExtentReport.stepName = "Click the login button";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.loginButton_Android);
		ExtentReport.stepName = "Click the wealth icon";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.wealth_Android);
		ExtentReport.stepName = "Click buy mutual funds chevron";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.bmfChevron);
		ExtentReport.stepName = "Navigate to omnibus flow";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.omnibusChevron);
		ExtentReport.stepName = "Click select fund button";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.selectFund);
		ExtentReport.stepName = "Select the account";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.acctSelection);
		ExtentReport.stepName = "Click the forward button";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.forwardBtn);
		Thread.sleep(10000);
		CommonMethods.sendKeysTextBox(TH_MF_OmnibusFlow_Objects.textInput, "1000000");
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.txtInputForwardBtn);
		Thread.sleep(20000);
		
	}

	@When("^To verify whether the application is eMFAccountOpening (\\d+) Back getting navigated to the required screen eMutualFundTrade (\\d+) on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_eMFAccountOpening_Back_getting_navigated_to_the_required_screen_eMutualFundTrade_on_clicking_th_provided_image_or_Button(int arg1, int arg2) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is eMFAccountOpening Back getting navigated to the required screen eMutualFundTrade on clicking th provided image or Button";
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.backBtn);
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.txtInputForwardBtn);
	}

	@And("^To Verify whether the required mandatory Field Mutual funds account opening is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_Mutual_funds_account_opening_is_getting_displayed_as_per_the_condition() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field Mutual funds account opening is getting displayed as per the condition";
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.header);
	}

	@Then("^To Verify whether the required mandatory Field To proceed with your order we will be opening a Mutual Funds account for you By default all dividends and returns will be credited to the following accounts is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_To_proceed_with_your_order_we_will_be_opening_a_Mutual_Funds_account_for_you_By_default_all_dividends_and_returns_will_be_credited_to_the_following_accounts_is_getting_displayed_as_per_the_condition() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field To proceed with your order we will be opening a Mutual Funds account for you By default all dividends and returns will be credited to the following accounts is getting displayed as per the condition";
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.desc1);
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.desc2);
	}

	@Then("^To verify whether the For funds in fundCurrency accountDesc and maskedAccountNumber is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_For_funds_in_fundCurrency_accountDesc_and_maskedAccountNumber_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the For funds in fundCurrency accountDesc and maskedAccountNumber is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.fundsInXXX);
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.acctNum);
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.accNum);
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.tickSymbol);
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.closeBtn);
	}

	@Then("^To verify whether the application is eMFAccountOpening (\\d+) PDF Icon and Terms and Conditions getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_eMFAccountOpening_PDF_Icon_and_Terms_and_Conditions_getting_navigated_to_the_required_screen_Open_corresponding_PDF_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is eMFAccountOpening PDF Icon and Terms and Conditions getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button";
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.termsAndConditions);
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.pdfIcon);
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.pdfBackBtn);
		
	}

	@Then("^To verify whether the application is eMFAccountOpening (\\d+) PDF Icon and Important document getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_eMFAccountOpening_PDF_Icon_and_Important_document_getting_navigated_to_the_required_screen_Open_corresponding_PDF_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is eMFAccountOpening PDF Icon and Important document getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button";
		CommonMethods.isElementPresent(TH_MF_OmnibusFlow_Objects.impDoc);
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.pdfIcon);
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.pdfBackBtn);
	}
	
	@When("^To verify whether the application is eMFAccountOpening (\\d+) Next getting navigated to the required screen on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_eMFAccountOpening_Next_getting_navigated_to_the_required_screen_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is eMFAccountOpening Next getting navigated to the required screen on clicking th provided image or Button";
		CommonMethods.clickMethodMultipleSession(TH_MF_OmnibusFlow_Objects.nextButton);
	}
	

}
