package com.citi.mobileAutomation.runnerClass;

import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.objectRepository.TH_MF_InputScreen_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TH_MF_InputScreen_StepDef {

	@Given("^Login with mrc customer$")
	public void login_with_mrc_customer() throws Throwable {

		OpeningAppiumServer.startAppiumServer();
		ExtentReport.stepName = "Login with mrc customer";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.continue_Android);
		ExtentReport.stepName = "Click the respective language";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.langBtn);
		ExtentReport.stepName = "Click accept button";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.acceptBtn);
		ExtentReport.stepName = "Click log into existing";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.existingLoginBtn);
		ExtentReport.stepName = "Enter the user id";
		CommonMethods.sendKeysTextBox(TH_MF_InputScreen_Objects.username_Android, Xls_Reader.userID);
		ExtentReport.stepName = "Enter the password";
		CommonMethods.sendKeysTextBox(TH_MF_InputScreen_Objects.password_Android, Xls_Reader.password);
		ExtentReport.stepName = "Click the login button";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.loginButton_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.getStarted_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.enableButton_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.alert_Ok_Btn_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.experienceCitiMobile_Android);
//		CommonMethods.clickMethodMultipleSession(Inputs_MF_Objects.experienceCitiMobile_Android);
		Thread.sleep(9000);
		ExtentReport.stepName = "Click on investment down chevron";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.investmentsdownChevron);
		ExtentReport.stepName = "Click on mutual fund chevron";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.mutualFundChevron);
		ExtentReport.stepName = "Click on holdings chevron";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.holdingsChevron);
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
		CommonMethods.scrollByJavaScriptExe();
		ExtentReport.stepName = "Click on an account";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.acctSelection);
		ExtentReport.stepName = "Click on sell button";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.sellBtn);
		Thread.sleep(9000);
	}

	@When("^To verify whether the application is Initiate Mutual Fund Sell Back getting navigated to the required screen AccountDetails (\\d+) on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_Initiate_Mutual_Fund_Sell_Back_getting_navigated_to_the_required_screen_AccountDetails_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		ExtentReport.stepName = "To verify whether the application is Initiate Mutual Fund Sell Back getting navigated to the required screen AccountDetails 125 on clicking th provided image or Button";
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.backButton);
		CommonMethods.clickMethodMultipleSession(TH_MF_InputScreen_Objects.sellBtn);
	}

	@Then("^To verify whether the How many units do you want to sell And noOfUnitsToSell is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_How_many_units_do_you_want_to_sell_And_noOfUnitsToSell_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the How many units do you want to sell And noOfUnitsToSell is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.unitsTosell);
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.inputAmt);
	}

	@And("^To verify whether the Indicative NAV indicativeNAV as of navLastUpdateDate is displayed details are matching with the API Response API Field$")
	public void to_verify_whether_the_Indicative_NAV_indicativeNAV_as_of_navLastUpdateDate_is_displayed_details_are_matching_with_the_API_Response_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Indicative NAV indicativeNAV as of navLastUpdateDate is displayed details are matching with the API Response API Field";
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.indicativeNav);
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.indicativeNavamt);
	}



	@And("^To verify whether the Unit held and fundUnitsAvailable is displayed details are matching with the API Responses API Field$")
	public void to_verify_whether_the_Unit_held_and_fundUnitsAvailable_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the Unit held and fundUnitsAvailable is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.unitsHeld);
	}

	@And("^To verify whether the invCurrency and invSellAmount is displayed detail are matching with the API Responses API Field$")
	public void to_verify_whether_the_invCurrency_and_invSellAmount_is_displayed_details_are_matching_with_the_API_Responses_API_Field() throws Throwable {
		ExtentReport.stepName = "To verify whether the invCurrency and invSellAmount is displayed details are matching with the API Responses API Field";
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.currencyAndAmt);
	}

	@And("^To Verify whether the required mandatory Field The amount shown above is the indicative it does not include redemption charge if the fund is applicable is getting displayed as per the condition$")
	public void to_Verify_whether_the_required_mandatory_Field_The_amount_shown_above_is_the_indicative_it_does_not_include_redemption_charge_if_the_fund_is_applicable_is_getting_displayed_as_per_the_condition() throws Throwable {
		ExtentReport.stepName = "To Verify whether the required mandatory Field The amount shown above is the indicative it does not include redemption charge if the fund is applicable is getting displayed as per the condition";
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.description);
	}

	@When("^To verify whether the application is InitiateMutualFundSell Forward getting navigated to the required screen eMutualFundSell(\\d+) on clicking th provided image or Button$")
	public void to_verify_whether_the_application_is_InitiateMutualFundSell_Forward_getting_navigated_to_the_required_screen_eMutualFundSell_on_clicking_th_provided_image_or_Button(int arg1) throws Throwable {
		Thread.sleep(10000);
		ExtentReport.stepName = "To verify whether the application is InitiateMutualFundSell Forward getting navigated to the required screen eMutualFundSell600 on clicking th provided image or Button";
		CommonMethods.sendKeysTextBoxMultipleSession(TH_MF_InputScreen_Objects.inputAmt, "100000000");
		Thread.sleep(10000);
		CommonMethods.isElementPresent(TH_MF_InputScreen_Objects.forwardButton);
	}
	
}
