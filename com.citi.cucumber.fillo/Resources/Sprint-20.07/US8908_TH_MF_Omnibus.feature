@All
Feature: Test MF as Account opening Omnibus flow for TH country

@AcctOpening_omnibus
Scenario: Verify MF as Account opening Omnibus flow functionality

#AUTHOR:Sangavi_Product:Acct opening omnibus

Given Login with MRC customer for account opening
When To verify whether the application is eMFAccountOpening 200 Back getting navigated to the required screen eMutualFundTrade 300 on clicking th provided image or Button
Then To Verify whether the required mandatory Field Mutual funds account opening is getting displayed as per the condition
And To Verify whether the required mandatory Field To proceed with your order we will be opening a Mutual Funds account for you By default all dividends and returns will be credited to the following accounts is getting displayed as per the condition
And To verify whether the For funds in fundCurrency accountDesc and maskedAccountNumber is displayed details are matching with the API Responses API Field
When To verify whether the application is eMFAccountOpening 200 PDF Icon and Terms and Conditions getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button
And To verify whether the application is eMFAccountOpening 200 PDF Icon and Important document getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button
And To verify whether the application is eMFAccountOpening 200 Next getting navigated to the required screen on clicking th provided image or Button