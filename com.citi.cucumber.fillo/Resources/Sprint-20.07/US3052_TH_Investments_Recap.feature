@All
Feature: Test Recap screen display for TH country

@Recap_ScreenDisplay
Scenario: Verify Recap screen display


Given Login with mrc customer Recap
When To verify whether the application is Recap Mutual Fund Sell Back getting navigated to the required screen eMutualFundSell 200 on clicking th provided image or Button
Then To Verify whether the required mandatory Field Review and confirm is getting displayed as per the condition
When To verify whether the application is Recap Mutual Fund Sell Close X getting navigated to the required screen Dashboard on clicking th provided image or Button
Then To verify whether the You have a sell order of fundName is displayed details are matching with the API Responses API Field
And To verify whether the You have redeemed fundUnitsRedeemed units is displayed details are matching with the API Responses API Field
And To verify whether the Order date and sellOrderDate and Trade will be completed within 3 working days is displayed details are matching with the API Responses API Field
When To verify whether the application is Recap Mutual Fund Sell Credit to account number settlementAccountName and settlementAccountNumber getting navigated to the required screen Display Change Settlement Account block on clicking th provided image or Button
Then To verify whether the Credit to account number settlementAccountName and settlementAccountNumber is displayed details are matching with the API Responses API Field
And To verify whether the settlementAccountName and settlementAccountNumber is displayed details are matching with the API Responses API Field
#Then To Verify whether the required mandatory Field Trade will be placed the next business day and Please note that the transaction will be for the next business days trade as it is currently past the funds transaction cutoff time is getting displayed as per the condition
#And To Verify whether the required mandatory Field Proceed is getting displayed as per the condition
#Then To verify whether the application is Recap Mutual Fund Sell Proceed getting navigated to the required screen eMutualFundSell 700 on clicking th provided image or Button
#And To Verify whether the required mandatory Field Cancel is getting displayed as per the condition
#Then To verify whether the application is Recap Mutual Fund Sell Cancel getting navigated to the required screen eMutualFundSell 200 on clicking th provided image or Button
And To verify whether the Amount credited S$ and sellAmountCredited The amount shown includes a charge of 1 redemption fee which is applicabe to this mutual fund is displayed details are matching with the API Responses API Field
And To verify whether the Fund Code and fundCode is displayed details are matching with the API Responses API Field
And To verify whether the Risk rating and fundRiskRating is displayed details are matching with the API Responses: API Field
And To verify whether the Available units and fundAvailableUnits is displayed details are matching with the API Responses API Field
And To verify whether the Indicative Nav fundIndicativeNAV as of navLastUpdateDate is displayed details are matching with the API Responses APIField
And To verify whether the Fund currency type and fundCurrencyType is displayed details are matching with the API Responses API Field
And To Verify whether the required mandatory Field Transaction type Non advised is getting displayed as per the condition
And To Verify whether the required mandatory Field By tapping Confirm you acknowledge and agree with Citi Singapore Limited to is getting displayed as per the condition
And To Verify whether the required mandatory Field PDF Icon is getting displayed as per the condition
When To verify whether the application is Recap Mutual Fund Sell Terms and Conditions getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button
And To verify whether the application is Recap Mutual Fund Sell Disclosure St getting navigated to the required screen Open corresponding PDF on clicking th provided image or Button
And To verify whether the application is Recap Mutual Fund Sell Confirm getting navigated to the required screen eMutualFundSell 700 Display Cut-off time exceeded block on clicking th provided image or Button